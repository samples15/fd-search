import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';

Vue.directive('debug-pre', {
  bind: (el) => {
    el.classList.add('pre');
  },
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
