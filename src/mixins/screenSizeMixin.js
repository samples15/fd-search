import BREAKPOINTS from './breakpoints';

const getScreenSize = () => {
  let width = BREAKPOINTS.xxl;
  let height = 0;

  width = window.innerWidth;
  height = window.innerHeight;

  return {
    width,
    height,
    minWidth: {
      sm: width >= BREAKPOINTS.sm,
      md: width >= BREAKPOINTS.md,
      lg: width >= BREAKPOINTS.lg,
      xl: width >= BREAKPOINTS.xl,
      xxl: width >= BREAKPOINTS.xxl,
    },
    maxWidth: {
      sm: width < BREAKPOINTS.sm,
      md: width < BREAKPOINTS.md,
      lg: width < BREAKPOINTS.lg,
      xl: width < BREAKPOINTS.xl,
      xxl: width < BREAKPOINTS.xxl,
      globalHeaderBreakpoint: width < 1400,
    },
  };
};

const screenSizeMixin = {
  data() {
    return {
      screenSize: getScreenSize(),
    };
  },
  created() {
    if (process.browser) {
      this.updateScreenSize();
      window.addEventListener('resize', this.onWindowResize);
    }
  },
  destroyed() {
    window.removeEventListener('resize', this.onWindowResize);
  },
  methods: {
    updateScreenSize() {
      this.screenSize = getScreenSize();
    },
    onWindowResize() {
      this.updateScreenSize();
    },
  },
};

export default screenSizeMixin;
