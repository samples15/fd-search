import Vue from 'vue';
import Vuex from 'vuex';

// Mocks
import dealsModel from '../mocks/deals.json';
import docsModel from '../mocks/documents.json';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // Mocks
    deals: dealsModel.data.Holdings,
    docs: docsModel.data.docs,

    // Data
    fullData: [],
  },
  mutations: {
    setPayload(state, model) {
      state.fullData = model;
    },
  },
  actions: {
    getModel({ commit, rootState }) {
      const arr = [];
      rootState.deals.forEach((deal) => {
        const matchedDocs = rootState.docs.filter((doc) => doc.holding_id === deal.Id);
        arr.push({
          ...deal,
          docs: matchedDocs,
        });
      });
      commit('setPayload', arr);
    },

    sortModel({ commit, rootState }, sort) {
      let sortedArr;

      if (sort === 'Z-A') {
        sortedArr = rootState.fullData.sort((a, b) => (a.DealName.toLowerCase() > b.DealName.toLowerCase() ? -1 : 1));
      } else if (sort === 'A-Z') {
        sortedArr = rootState.fullData.sort((a, b) => (b.DealName.toLowerCase() > a.DealName.toLowerCase() ? -1 : 1));
      } else {
        sortedArr = rootState.fullData;
      }

      commit('setPayload', sortedArr);
    },

    filterByAgent({ commit, rootState }, filter) {
      const filteredArr = rootState.fullData.filter((deal) => deal.docs.find((doc) => doc.holding.agent_name.toLowerCase() === filter.toLowerCase()));
      commit('setPayload', filteredArr);
    },

    filterByIndustry({ commit, rootState }, filter) {
      const filteredArr = rootState.fullData.filter((deal) => deal.docs.find((doc) => doc.holding.industry_name === filter));
      commit('setPayload', filteredArr);
    },
  },
});
